Name:           cvmfs-mount-point
Version:        XXX
Release:        1%{?dist}
Summary: Create an empty dir in / for cvmfs

License: MIT
URL: cern.ch            
Source0: cvmfs-mount-point-XXX.tar.gz
Source2: unlock-cvmfs.service
Source3: lock-cvmfs.service
Source4: 92-cvmfs-ostree-integration.preset

BuildRequires: systemd

%description


%prep


%build


%install
rm -rf $RPM_BUILD_ROOT
#mkdir -p %{buildroot}/usr/opt/cvmfs

install -p -D -m 644 %{SOURCE2} %{buildroot}/etc/systemd/system/unlock-cvmfs.service
install -p -D -m 644 %{SOURCE3} %{buildroot}/etc/systemd/system/lock-cvmfs.service
install -p -D -m 644 %{SOURCE4} %{buildroot}%{_prefix}/lib/systemd/system-preset/92-cvmfs-ostree-integration.preset

install -d -m 0755 %{buildroot}/usr/lib/tmpfiles.d

echo "d /var/cvmfs  755 root root" > %{buildroot}%{_prefix}/lib/tmpfiles.d/cvmfs-ostree-integration.conf
echo "L+ /cvmfs - - - - /var/cvmfs" >> %{buildroot}%{_prefix}/lib/tmpfiles.d/cvmfs-ostree-integration.conf


%files
/etc/systemd/system/unlock-cvmfs.service
/etc/systemd/system/lock-cvmfs.service

%{_prefix}/lib/systemd/system-preset/92-cvmfs-ostree-integration.preset
%{_prefix}/lib/tmpfiles.d/cvmfs-ostree-integration.conf
#%dir /usr/opt/cvmfs

%post
%systemd_post unlock-cvmfs.service
%systemd_post lock-cvmfs.service

%preun
%systemd_preun unlock-cvmfs.service
%systemd_preun lock-cvmfs.service

%postun
%systemd_postun_with_restart unlock-cvmfs.service
%systemd_postun_with_restart lock-cvmfs.service

%changelog
* Mon Jun 20 2016 Fedora Cloud User
-
